from flask import Blueprint, request, render_template, redirect, url_for, session
from jettronics.controllers import AuthController
from jettronics.login_manager import LoginManager

# Blueprint
bp = Blueprint('auth', __name__)

# Controller
controller = AuthController()


@bp.route('/auth/register', methods=['GET', 'POST'])
def register():
    # POST
    if request.method == 'POST':
        # Request form data
        # Name
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')

        # Gender, Date of Birth, and Ethnicity
        gender = request.form.get('gender')
        date_of_birth = request.form.get('date_of_birth')
        ethnicity = request.form.get('ethnicity')

        # Email Address and Phone Number
        email_address = request.form.get('email_address')
        phone_number = request.form.get('phone_number')

        # Address
        street_address = request.form.get('street_address')
        city = request.form.get('city')
        postal_code = request.form.get('postal_code')
        province = request.form.get('province')
        country = request.form.get('country')

        # Username and Passwords
        username = request.form.get('username')
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')

        # Form Data
        form_data = {
            'first_name': first_name, 'last_name': last_name,
            'gender': gender, 'date_of_birth': date_of_birth, 'ethnicity': ethnicity,
            'email_address': email_address, 'phone_number': phone_number, 
            'street_address': street_address, 'city': city, 'postal_code': postal_code, 'province': province, 'country': country,
            'username': username, 'password': password, 'confirm_password': confirm_password
        }

        errors = controller.register(form_data)
        if len(errors) > 0:
            return render_template('auth/register.html', title='JetTronics - Registration', errors=errors)
        else:
            return redirect(url_for('auth.login'))

    return render_template('auth/register.html', title='JetTronics - Register')


@bp.route('/auth/login', methods=['GET', 'POST'])
def login():
    # POST
    if request.method == 'POST':
        # Request form data
        username = request.form.get('username')
        password = request.form.get('password')

        # Form data
        form_data = {
            'username': username,
            'password': password
        }

        # Get errors using controller
        errors = controller.login(form_data)

        # Check if there are any errors
        if len(errors) > 0:
            return render_template('auth/login.html', title='JeTronics - Login', errors=errors)
        else:
            return redirect(url_for('index'))

    if request.args.get('errors') is not None:
        error = request.args.get('errors')
        return render_template('auth/login.html', title='JetTronics - Login', error=error)
    return render_template('auth/login.html', title='JetTronics - Login')


@bp.route('/auth/login-required')
def login_required():
    error = 'You must be logged in to view the requested page.'
    errors = []
    errors.append(error)
    return redirect(url_for('auth.login', errors=errors))


@bp.route('/auth/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.login'))


# Authenticate User
def authenticate():
    def is_logged_in():
        if 'username' in session:
            return True
        else:
            return False
    return is_logged_in()
