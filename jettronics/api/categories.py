import math
from flask import Blueprint, request, jsonify, redirect, url_for
from jettronics.controllers import CategoriesController
from jettronics.login_manager import LoginManager

bp = Blueprint('categories_api', __name__)
controller = CategoriesController()
login_manager = LoginManager()


# Fetches all category records
@bp.route('/api/categories/fetch-all')
def fetch_all():
    
    # Authenticate
    if login_manager.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    categories = controller.select_all_as_dict()
    
    return jsonify(categories)


# Fetches a single category by its ID
@bp.route('/api/categories/fetch-by-id/<category_id>')
def fetch_by_id(category_id):
    
    # Authenticate
    if login_manager.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    category = controller.select_by_id_as_dict(category_id)
    
    return jsonify(category)


# Fetches categories with a start and end index
@bp.route('/api/categories/fetch-category-pages/<limit>')
def fetch_pages(limit):
    # categories = controller.select_in_range(start_index, end_index)
    
    pages = controller.get_pages(limit)
    
    return jsonify(pages)


@bp.route('/api/categories/fetch-in-range/<page>/<limit>')
def fetch_in_range(page, limit):
    page = int(page)
    limit = int(limit)
    
    pages = controller.get_pages(limit)
    
    page_index = page - 1
    page_data = None
    
    try:
        page_data = pages[page_index]
    except:
        pass
    
    return jsonify(page_data)


@bp.route('/api/categories/fetch-page-count/<limit>')
def fetch_page_count(limit):
    categories = controller.select_all()
    page_count = math.ceil(len(categories) / limit)