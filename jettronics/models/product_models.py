class Product():
    
    def __init__(self, name, category_name, description, warehouse_prefix, location, price_per_unit, 
                 quantity, sku, barcode, country_of_origin, notes):
        self._id = None
        self._name = name
        self._category_name = category_name
        self._description = description
        self._warehouse_prefix = warehouse_prefix
        self._location = location
        self._price_per_unit = price_per_unit
        self._quantity = quantity
        self._sku = sku
        self._barcode = barcode
        self._country_of_origin = country_of_origin
        self._notes = notes
        self._created_at = None
        
    # ID
    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, id):
        self._id = id
        
    # Name
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, name):
        self._name = name
    
    # Category Name
    @property
    def category_name(self):
        return self._category_name
    
    @category_name.setter
    def category_name(self, category_name):
        self._category_name = category_name
    
    # Description
    @property
    def description(self):
        return self._description
    
    @description.setter
    def description(self, description):
        self._description = description
    
    # Warehouse Prefix
    @property
    def warehouse_prefix(self):
        return self._warehouse_prefix
    
    @warehouse_prefix.setter
    def warehouse_prefix(self, warehouse_prefix):
        self._warehouse_prefix = warehouse_prefix
    
    # Location
    @property
    def location(self):
        return self._location
    
    @location.setter
    def location(self, location):
        self._location = location
    
    # Price Per Unit
    @property
    def price_per_unit(self):
        return self._price_per_unit
    
    @price_per_unit.setter
    def price_per_unit(self, price_per_unit):
        self._price_per_unit = price_per_unit
    
    # Quantity
    @property
    def quantity(self):
        return self._quantity
    
    @quantity.setter
    def quantity(self, quantity):
        self._quantity = quantity
    
    # SKU
    @property
    def sku(self):
        return self._sku
    
    @sku.setter
    def sku(self, sku):
        self._sku = sku
    
    # Barcode
    @property
    def barcode(self):
        return self._barcode
    
    @barcode.setter
    def barcode(self, barcode):
        self._barcode = barcode
    
    # Country of Origin
    @property
    def country_of_origin(self):
        return self._country_of_origin
    
    @country_of_origin.setter
    def country_of_origin(self, country_of_origin):
        self._country_of_origin = country_of_origin
    
    # Notes
    @property
    def notes(self):
        return self._notes
    
    @notes.setter
    def notes(self, notes):
        self._notes = notes
    
    # Created At
    @property
    def created_at(self):
        return self._created_at
    
    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at
    
    # To Dict
    def to_dict(self):
        return self.__dict__