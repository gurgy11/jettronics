'''
People Models:
    - Person
    - Contact
    - ClientContact
    - SupplierContact
    - Employee
    - User
'''


from jettronics.models.associate_models import Associate


class Person():

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city, postal_code, province,
                 country, notes):
        self._id = None
        self._first_name = first_name
        self._last_name = last_name
        self._gender = gender
        self._date_of_birth = date_of_birth
        self._ethnicity = ethnicity
        self._email_address = email_address
        self._phone_number = phone_number
        self._street_address = street_address
        self._city = city
        self._postal_code = postal_code
        self._province = province
        self._country = country
        self._notes = notes
        self._created_at = None

    # Id
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    # First Name
    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        self._first_name = first_name

    # Last Name
    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        self._last_name = last_name

    # Gender
    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, gender):
        self._gender = gender

    # Date of Birth
    @property
    def date_of_birth(self):
        return self._date_of_birth

    @date_of_birth.setter
    def date_of_birth(self, date_of_birth):
        self._date_of_birth = date_of_birth

    # Ethnicity
    @property
    def ethnicity(self):
        return self._ethnicity

    @ethnicity.setter
    def ethnicity(self, ethnicity):
        self._ethnicity = ethnicity

    # Email Address
    @property
    def email_address(self):
        return self._email_address

    @email_address.setter
    def email_address(self, email_address):
        self._email_address = email_address

    # Phone Number
    @property
    def phone_number(self):
        return self._phone_number

    @phone_number.setter
    def phone_number(self, phone_number):
        self._phone_number = phone_number

    # Street Address
    @property
    def street_address(self):
        return self._street_address

    @street_address.setter
    def street_address(self, street_address):
        self._street_address = street_address

    # City
    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, city):
        self._city = city

    # Postal Code
    @property
    def postal_code(self):
        return self._postal_code

    @postal_code.setter
    def postal_code(self, postal_code):
        self._postal_code = postal_code

    # Province
    @property
    def province(self):
        return self._province

    @province.setter
    def province(self, province):
        self._province = province

    # Country
    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, country):
        self._country = country

    # Notes
    @property
    def notes(self):
        return self._notes

    @notes.setter
    def notes(self, notes):
        self._notes = notes

    # Created At
    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at

    # To Dict
    def to_dict(self):
        return self.__dict__


class User(Person):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, username, password, account_role='', notes=''):
        self._uid = None
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                         postal_code, province, country, notes)
        self._username = username
        self._password = password
        self._account_role = account_role
        self._notes = notes
        self._created_at = None

    # UID
    @property
    def uid(self):
        return self._uid
    
    @uid.setter
    def uid(self, uid):
        self._uid = uid

    # Username
    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, username):
        self._username = username

    # Password
    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    # Account Role
    @property
    def account_role(self):
        return self._account_role

    @account_role.setter
    def account_role(self, account_role):
        self._account_role = account_role

    # Notes
    @property
    def notes(self):
        return self._notes

    @notes.setter
    def notes(self, notes):
        self._notes = notes

    # Created At
    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at

    # To Dict
    def to_dict(self):
        return self.__dict__


class Contact(Person):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, notes, contact_type):
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city, postal_code,
                         province, country, notes)
        self._contact_type = contact_type

    # Contact Type
    @property
    def contact_type(self):
        return self._contact_type

    @contact_type.setter
    def contact_type(self, contact_type):
        self._contact_type = contact_type

    # To Dict
    def to_dict(self):
        return self.__dict__


class ClientContact(Contact):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, notes, contact_type, client_name):
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                         postal_code, province, country, notes, contact_type)
        self._client_name = client_name

    # Client Name
    @property
    def client_name(self):
        return self._client_name

    @client_name.setter
    def client_name(self, client_name):
        self._client_name = client_name

    # To Dict
    def to_dict(self):
        return self.__dict__


class SupplierContact(Contact):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, notes, contact_type, supplier_name):
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                         postal_code, province, country, notes, contact_type)
        self._supplier_name = supplier_name

    # Supplier Name
    @property
    def supplier_name(self):
        return self._supplier_name

    @supplier_name.setter
    def supplier_name(self, supplier_name):
        self._supplier_name = supplier_name

    # To Dict
    def to_dict(self):
        return self.__dict__


class Employee(Person):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, notes, job_title, warehouse_name, wage_unit, wage_per_unit, employed_at):
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                         postal_code, province, country, notes)
        self._job_title = job_title
        # The warehouse's name where the employee is stationed at
        self._warehouse_name = warehouse_name
        # Is the employee paid by the hour or do they have a set yearly wage?
        self._wage_unit = wage_unit
        self._wage_per_unit = wage_per_unit
        self._employed_at = employed_at  # Date when employee was first hired

    # Job Title
    @property
    def job_title(self):
        return self._job_title

    @job_title.setter
    def job_title(self, job_title):
        self._job_title = job_title

    # Warehouse Name
    @property
    def warehouse_name(self):
        return self._warehouse_name

    @warehouse_name.setter
    def warehouse_name(self, warehouse_name):
        self._warehouse_name = warehouse_name

    # Wage Unit
    @property
    def wage_unit(self):
        return self._wage_unit

    @wage_unit.setter
    def wage_unit(self, wage_unit):
        self._wage_unit = wage_unit

    # Wage Per Unit
    @property
    def wage_per_unit(self):
        return self._wage_per_unit

    @wage_per_unit.setter
    def wage_per_unit(self, wage_per_unit):
        self._wage_per_unit = wage_per_unit

    # Employed At
    @property
    def employed_at(self):
        return self._employed_at

    @employed_at.setter
    def employed_at(self, employed_at):
        self._employed_at = employed_at

    # To Dict
    def to_dict(self):
        return self.__dict__


class EmployeeUser(Employee):

    def __init__(self, first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                 postal_code, province, country, notes, job_title, warehouse_name, wage_unit, wage_per_unit, employed_at,
                 username, password, account_role, last_login_at):
        super().__init__(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city,
                         postal_code, province, country, notes, job_title, warehouse_name, wage_unit, wage_per_unit, employed_at)
        self._username = username
        self._password = password
        # Role determines account access level. Ex: Admin has access to all features
        self._account_role = account_role
        self._last_login_at = last_login_at

    # Username
    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, username):
        self._username = username

    # Password
    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password

    # Account Role
    @property
    def account_role(self):
        return self._account_role

    @account_role.setter
    def account_role(self, account_role):
        self._account_role = account_role

    # Last Login At
    @property
    def last_login_at(self):
        return self._last_login_at

    @last_login_at.setter
    def last_login_at(self, last_login_at):
        self._last_login_at = last_login_at

    # To Dict
    def to_dict(self):
        return self.__dict__
