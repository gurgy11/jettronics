'''
Category Models:
    - Category
    - SubCategory
'''


class Category():

    def __init__(self, name, description, notes):
        self._id = None
        self._name = name
        self._description = description
        self._notes = notes
        self._created_at = None

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def description(self):
        return self._description

    @property
    def notes(self):
        return self._notes

    @property
    def created_at(self):
        return self._created_at

    @id.setter
    def id(self, id):
        self._id = id

    @name.setter
    def name(self, name):
        self._name = name

    @description.setter
    def description(self, description):
        self._description = description

    @notes.setter
    def notes(self, notes):
        self._notes = notes

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at

    def to_dict(self):
        return self.__dict__


class SubCategory(Category):

    def __init__(self, name, description, notes, parent_category_id):
        super().__init__(name, description, notes)
        self._parent_category_id = parent_category_id

    @property
    def parent_category_id(self):
        return self._parent_category_id

    @parent_category_id.setter
    def parent_category_id(self, parent_category_id):
        self._parent_category_id = parent_category_id