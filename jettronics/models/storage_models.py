''' 
Storage Models:
    - Storage
    - Warehouse
    - Location
'''


class Storage():

    def __init__(self, name, description, notes):
        self._id = None
        self._name = name
        self._description = description
        self._notes = notes
        self._created_at = None

    # Id
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    # Name
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    # Description
    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    # Notes
    @property
    def notes(self):
        return self._notes

    @notes.setter
    def notes(self, notes):
        self._notes = notes

    # Created At
    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at

    # To Dict
    def to_dict(self):
        return self.__dict__


class Warehouse(Storage):

    def __init__(self, name, description, notes, prefix, street_address, city, postal_code, province, country, manager_name, manager_email, manager_phone):
        super().__init__(name, description, notes)
        self._prefix = prefix
        self._street_address = street_address
        self._city = city
        self._postal_code = postal_code
        self._province = province
        self._country = country
        self._manager_name = manager_name
        self._manager_email = manager_email
        self._manager_phone = manager_phone

    # Prefix
    @property
    def prefix(self):
        return self._prefix

    @prefix.setter
    def prefix(self, prefix):
        self._prefix = prefix

    # Street Address
    @property
    def street_address(self):
        return self._street_address

    @street_address.setter
    def street_address(self, street_address):
        self._street_address = street_address

    # City
    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, city):
        self._city = city

    # Postal Code
    @property
    def postal_code(self):
        return self._postal_code

    @postal_code.setter
    def postal_code(self, postal_code):
        self._postal_code = postal_code

    # Province
    @property
    def province(self):
        return self._province

    @province.setter
    def province(self, province):
        self._province = province

    # Country
    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, country):
        self._country = country

    # Manager Name
    @property
    def manager_name(self):
        return self._manager_name

    @manager_name.setter
    def manager_name(self, manager_name):
        self._manager_name = manager_name

    # Manager Email
    @property
    def manager_email(self):
        return self._manager_email

    @manager_email.setter
    def manager_email(self, manager_email):
        self._manager_email = manager_email

    # Manager Phone
    @property
    def manager_phone(self):
        return self._manager_phone

    @manager_phone.setter
    def manager_phone(self, manager_phone):
        self._manager_phone = manager_phone

    # To Dict
    def to_dict(self):
        return self.__dict__


class Location(Storage):

    def __init__(self, name, description, notes, warehouse_prefix, available=False, product_sku='', product_quantity=0):
        super().__init__(name, description, notes)
        self._warehouse_prefix = warehouse_prefix
        self._available = available
        self._product_sku = product_sku
        self._product_quantity = product_quantity

    # Warehouse Name
    @property
    def warehouse_prefix(self):
        return self._warehouse_prefix

    @warehouse_prefix.setter
    def warehouse_name(self, warehouse_prefix):
        self._warehouse_prefix = warehouse_prefix

    # Available
    @property
    def available(self):
        return self._available

    @available.setter
    def available(self, available):
        self._available = available

    # Product SKU
    @property
    def product_sku(self):
        return self._product_sku

    @product_sku.setter
    def product_sku(self, product_sku):
        self._product_sku = product_sku

    # Product Quantity
    @property
    def product_quantity(self):
        return self._product_quantity

    @product_quantity.setter
    def product_quantity(self, product_quantity):
        self._product_quantity = product_quantity

    # To Dict
    def to_dict(self):
        return self.__dict__