'''
Associate Models:
    - Associate
    - Client
    - Supplier
'''


class Associate():

    def __init__(self, name, street_address, city, postal_code, province, country, notes):
        self._id = None
        self._name = name
        self._street_address = street_address
        self._city = city
        self._postal_code = postal_code
        self._province = province
        self._country = country
        self._notes = notes
        self._created_at = None

    # Id
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    # Name
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    # Street Address
    @property
    def street_address(self):
        return self._street_address

    @street_address.setter
    def street_address(self, street_address):
        self._street_address = street_address

    # City
    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, city):
        self._city = city

    # Postal Code
    @property
    def postal_code(self):
        return self._postal_code

    @postal_code.setter
    def postal_code(self, postal_code):
        self._postal_code = postal_code

    # Province
    @property
    def province(self):
        return self._province

    @province.setter
    def province(self, province):
        self._province = province

    # Country
    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, country):
        self._country = country

    # Notes
    @property
    def notes(self):
        return self._notes

    @notes.setter
    def notes(self, notes):
        self._notes = notes

    # Created At
    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at

    def to_dict(self):
        return self.__dict__


class Client(Associate):

    def __init__(self, name, street_address, city, postal_code, province, country, notes, last_ordered_at):
        super().__init__(name, street_address, city, postal_code, province, country, notes)
        self._last_ordered_at = last_ordered_at

    # Last Ordered At
    @property
    def last_ordered_at(self):
        return self._last_ordered_at
    
    @last_ordered_at.setter
    def last_ordered_at(self, last_ordered_at):
        self._last_ordered_at = last_ordered_at

    def to_dict(self):
        return self.__dict__


class Supplier(Associate):

    def __init__(self, name, street_address, city, postal_code, province, country, notes, last_purchased_at):
        super().__init__(name, street_address, city, postal_code, province, country, notes)
        self._last_purchased_at = last_purchased_at

    # Last Purchased At
    @property
    def last_purchased_at(self):
        return self._last_purchased_at
    
    @last_purchased_at.setter
    def last_purchased_at(self, last_purchased_at):
        self._last_purchased_at = last_purchased_at

    def to_dict(self):
        return self.__dict__