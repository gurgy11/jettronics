from jettronics.controllers import Controller
from jettronics.controllers import CategoriesController
from jettronics.db import Database
from jettronics.models import Product
from jettronics.utils import Paginator


class ProductsController(Controller):
    
    def __init__(self):
        self.db = Database()
        self.table_name = 'products'
        self.columns = [
            'name', 'category_name',
            'description',
            'warehouse_prefix', 'location',
            'price_per_unit', 'quantity',
            'sku', 'barcode',
            'country_of_origin',
            'notes'
        ]
        self.paginator = Paginator(module_name='products')
        self.category_controller = CategoriesController()
        
    def result_to_model(self, result):
        pid = result[0]
        name = result[1]
        category_name = result[2]
        description = result[3]
        warehouse_prefix = result[4]
        location = result[5]
        price_per_unit = result[6]
        quantity = result[7]
        sku = result[8]
        barcode = result[9]
        country_of_origin = result[10]
        notes = result[11]
        created_at = result[12]
        
        product = Product(name, category_name, description, warehouse_prefix, location, 
                          price_per_unit, quantity, sku, barcode, country_of_origin, notes)
        product.id = pid
        product.created_at = created_at
        
        return product
    
    def result_to_dict(self, result):
        # Result data
        pid = result[0]
        name = result[1]
        category_name = result[2]
        description = result[3]
        warehouse_prefix = result[4]
        location = result[5]
        price_per_unit = result[6]
        quantity = result[7]
        sku = result[8]
        barcode = result[9]
        country_of_origin = result[10]
        notes = result[11]
        created_at = result[12]
        
        product = {
            'id': pid,
            'name': name,
            'category_name': category_name,
            'description': description,
            'warehouse_prefix': warehouse_prefix, 'location': location,
            'price_per_unit': price_per_unit, 'quantity': quantity,
            'sku': sku, 'barcode': barcode,
            'country_of_origin': country_of_origin,
            'notes': notes,
            'created_at': created_at,
        }
        
        return product
    
    def form_data_to_model(self, form_data):
        name = form_data.get('name')
        category_name = form_data.get('category_name')
        description = form_data.get('description')
        warehouse_prefix = form_data.get('warehouse_prefix')
        location = form_data.get('location')
        price_per_unit = form_data.get('price_per_unit')
        quantity = form_data.get('quantity')
        sku = form_data.get('sku')
        barcode = form_data.get('barcode')
        country_of_origin = form_data.get('country_of_origin')
        notes = form_data.get('notes')
        
        product = Product(name, category_name, description, warehouse_prefix, location, 
                          price_per_unit, quantity, sku, barcode, country_of_origin, notes)
        
        return product
    
    def form_data_to_values(self, form_data):
        product = self.form_data_to_model(form_data)
        values = (product.name, product.category_name, product.description, product.warehouse_prefix, 
                  product.location, product.price_per_unit, product.quantity, product.sku, 
                  product.barcode, product.country_of_origin, product.notes)
        
        return values
    
    def model_to_dict(self, product):
        product_dict = {
            'id': product.id,
            'name': product.name,
            'category_name': product.category_name,
            'description': product.description,
            'warehouse_prefix': product.warehouse_prefix,
            'location': product.location,
            'price_per_unit': product.price_per_unit,
            'quantity': product.quantity,
            'sku': product.sku,
            'barcode': product.barcode,
            'country_of_origin': product.country_of_origin,
            'notes': product.notes,
            'created_at': product.created_at
        }
        
        return product_dict
    
    def select_all(self):
        products = []
        results = self.db.select_all_records(self.table_name)
        
        for res in results:
            product = self.result_to_model(res)
            products.append(product)
            
        return products
    
    def select_by_id(self, product_id):
        result = self.db.select_record_by_id(self.table_name, product_id)
        product = self.result_to_model(result)
        
        return product
    
    def select_all_categories(self):
        categories = self.category_controller.select_all()
        return categories
    
    def select_all_as_dict(self):
        results = self.db.select_all_records(self.table_name)
        products = []
        
        for res in results:
            product = self.result_to_dict(res)
            products.append(product)
            
        return products
    
    def select_by_id_as_dict(self, product_id):
        result = self.db.select_record_by_id(self.table_name, product_id)
        product = self.result_to_dict(result)
        
        return product
    
    def name_exists(self, name):
        results = self.db.select_records_where_condition(self.table_name, 'name', name)
        
        name_exists = False
        if len(results) > 0:
            name_exists = True
        
        return name_exists
    
    def sku_exists(self, sku):
        sku_exists = False
        
        results = self.db.select_records_where_condition(self.table_name, 'sku', sku)

        if len(results) > 0:
            sku_exists = True
            
        return sku_exists
    
    def barcode_exists(self, barcode):
        barcode_exists = False
        results = self.db.select_records_where_condition(self.table_name, 'barcode', barcode)
        
        if len(results) > 0:
            barcode_exists = True
        
        return barcode_exists
    
    def create(self, form_data):
        errors = []
        
        # Check if name already exists
        if self.name_exists(form_data.get('name')) is True:
            error = 'The name provided is already being used by another product record!'
            errors.append(error)
        
        # Check if sku already exists
        if self.sku_exists(form_data.get('sku')) is True:
            error = 'The SKU provided is already being used by another product record!'
            errors.append(error)
            
        # Check if barcode already exists
        if self.barcode_exists(form_data.get('barcode')) is True:
            error = 'The barcode provided is already being used by another product record!'
            errors.append(error)
            
        # Create product if there are no errors
        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.insert_single_record(self.table_name, self.columns, values)
        
        return errors
    
    def delete(self, product_id):
        self.db.delete_single_record(self.table_name, product_id)
        
    def edit(self, product_id, form_data):
        values = self.form_data_to_values(form_data)
        self.db.update_single_record(self.table_name, self.columns, values, product_id)
        
        # Todo: Validate
        errors = []
        
        return errors
    
    def select_in_range(self, page_number, limit_per_page):
        all_products = self.select_all()
        
        items_count = self.paginator.get_number_of_items(all_products)
        pages_count = self.paginator.get_number_of_pages(items_count, limit_per_page)
        pages = self.paginator.get_page_index_ranges(pages_count, limit_per_page)
        page = pages[int(page_number) - 1]
        
        products = []
        
        start_index = page.get('start_index')
        end_index = page.get('end_index')
        
        for p in range(start_index, end_index):
            try:
                product = all_products[p]
                product_dict = self.model_to_dict(product)
                products.append(product_dict)
            except:
                break;
        
        return products
    
    def number_of_products(self):
        products = self.select_all()
        return len(products)
    
    def get_number_of_pages(self, limit_per_page):
        products = self.select_all()
        number_of_products = len(products)
        page_count = self.paginator.get_number_of_pages(number_of_products, limit_per_page)
        return page_count