from werkzeug.security import check_password_hash, generate_password_hash
from jettronics.db import Database
from jettronics.models import User
from jettronics.controllers import Controller
from jettronics.login_manager import LoginManager


class AuthController(Controller):

    def __init__(self):
        self.db = Database()
        self.table_name = 'users'
        self.columns = ['first_name', 'last_name', 'gender', 'date_of_birth', 'ethnicity', 'email_address', 'phone_number', 'street_address', 'city',
                        'postal_code', 'province', 'country', 'notes', 'job_title', 'warehouse_name', 'wage_unit', 'wage_per_unit', 'employed_at',
                        'username', 'password', 'account_role', 'last_login_at']
        self.login_manager = LoginManager()

    def form_data_to_model(self, form_data):
        # Form data values
        first_name = form_data.get('first_name')
        last_name = form_data.get('last_name')
        gender = form_data.get('gender')
        data_of_birth = form_data.get('date_of_birth')
        ethnicity = form_data.get('ethnicity')
        email_address = form_data.get('email_address')
        phone_number = form_data.get('phone_number')
        street_address = form_data.get('street_address')
        city = form_data.get('city')
        postal_code = form_data.get('postal_code')
        province = form_data.get('province')
        country = form_data.get('country')
        username = form_data.get('username')
        password = form_data.get('password')

        # User model
        user = User(first_name, last_name, gender, data_of_birth, ethnicity, email_address, phone_number, street_address, city, postal_code, province, country,
                    username, password)

        return user

    def result_to_model(self, result):
        # Result data values
        uid = result[0]
        first_name = result[1]
        last_name = result[2]
        gender = result[3]
        date_of_birth = result[4]
        ethnicity = result[5]
        email_address = result[6]
        phone_number = result[7]
        street_address = result[8]
        city = result[9]
        postal_code = result[10]
        province = result[11]
        country = result[12]
        notes = result[13]
        username = result[14]
        password = result[15]
        account_role = result[16]
        created_at = result[17]

        # User model
        user = User(first_name, last_name, gender, date_of_birth, ethnicity, email_address, phone_number, street_address, city, postal_code, 
        province, country, username, password, account_role, notes)
        user.uid = uid
        user.created_at = created_at

        return user


    def username_exists(self, username):
        # Fetch records using username
        results = self.db.select_records_where_condition(
            self.table_name, 'username', username)

        username_exists = False

        if len(results) > 0:
            username_exists = True

        return username_exists

    def email_address_exists(self, email_address):
        email_address_exists = False
        results = self.db.select_records_where_condition(
            self.table_name, 'email_address', email_address)

        if len(results) > 0:
            email_address_exists = True

        return email_address_exists

    def passwords_match(self, password, confirm_password):
        passwords_match = False

        if password == confirm_password:
            passwords_match = True

        return passwords_match

    def password_is_valid(self, hashed_password, password):
        password_is_valid = check_password_hash(hashed_password, password)

        return password_is_valid

    def register(self, form_data):
        # Convert form into a user model
        user = self.form_data_to_model(form_data)

        # Confirm password value from form
        confirm_password = form_data.get('confirm_password')

        # Empty array for errors
        errors = []

        ### Validate form
        # Check if username exists
        if self.username_exists(user.username) is True:
            error = 'The username provided is already being used by another account.'
            errors.append(error)
        # Check if email address exists
        if self.email_address_exists(user.email_address) is True:
            error = 'The email address provided is already being used by another account.'
            errors.append(error)
        # Check if password fields match
        if user.password != confirm_password:
            error = 'The password fields provided must match.'
            errors.append(error)

        # Register if no errors
        if len(errors) <= 0:
            columns = [
                'first_name', 'last_name', 
                'gender', 'date_of_birth', 'ethnicity', 
                'email_address', 'phone_number', 
                'street_address', 'city', 'postal_code', 'province', 'country',
                'username', 'password'
            ]
            values = (
                user.first_name, user.last_name,
                user.gender, user.date_of_birth, user.ethnicity,
                user.email_address, user.phone_number,
                user.street_address, user.city, user.postal_code, user.province, user.country,
                user.username, generate_password_hash(user.password)
            )
            self.db.insert_single_record(self.table_name, columns, values)
            return errors
        return errors
    
    def login(self, form_data):
        # Form data values
        username = form_data.get('username')
        password = form_data.get('password')

        # Errors array
        errors = []

        # Empty result
        result = None

        # Check if username exists
        if self.username_exists(username) is True:
            # Select the records with the username
            results = self.db.select_records_where_condition(self.table_name, 'username', username)
            result = results[0]

            # Convert the first result to model
            user = self.result_to_model(result)

            # Check if password is valid
            if check_password_hash(user.password, password) is not True:
                error = 'The password provided is incorrect.'
                errors.append(error)
            else:
                self.login_manager.add_user_to_session(user.uid, user.username)
        else:
            error = 'The username provided does not exist.'
            errors.append(error)

        return errors