from jettronics.db import Database
from jettronics.models import Warehouse
from jettronics.controllers import Controller


class WarehousesController(Controller):

    def __init__(self):
        self.db = Database()
        self.table_name = 'warehouses'
        self.columns = ['name', 'description', 'notes', 'prefix', 'street_address', 'city', 'postal_code', 'province',
                        'country', 'manager_name', 'manager_email', 'manager_phone']

    def form_data_to_model(self, form_data):
        # Form data values
        name = form_data.get('name')
        description = form_data.get('description')
        notes = form_data.get('notes')
        prefix = form_data.get('prefix')
        street_address = form_data.get('street_address')
        city = form_data.get('city')
        postal_code = form_data.get('postal_code')
        province = form_data.get('province')
        country = form_data.get('country')
        manager_name = form_data.get('manager_name')
        manager_email = form_data.get('manager_email')
        manager_phone = form_data.get('manager_phone')

        # Warehouse model
        warehouse = Warehouse(name, description, notes, prefix, street_address, city, postal_code, province, country,
                              manager_name, manager_email, manager_phone)

        return warehouse

    def result_to_model(self, result):
        # Result data values
        wid = result[0]
        name = result[1]
        description = result[2]
        notes = result[3]
        prefix = result[4]
        street_address = result[5]
        city = result[6]
        postal_code = result[7]
        province = result[8]
        country = result[9]
        manager_name = result[10]
        manager_email = result[11]
        manager_phone = result[12]
        created_at = result[13]

        # Warehouse model
        warehouse = Warehouse(name, description, notes, prefix, street_address, city, postal_code, province, country,
                              manager_name, manager_email, manager_phone)
        warehouse.id = wid
        warehouse.created_at = created_at

        return warehouse

    def form_data_to_values(self, form_data):
        warehouse = self.form_data_to_model(form_data)
        values = (warehouse.name, warehouse.description, warehouse.notes, warehouse.prefix, warehouse.street_address, warehouse.city,
                  warehouse.postal_code, warehouse.province, warehouse.country, warehouse.manager_name, warehouse.manager_email, warehouse.manager_phone)

        return values

    def name_exists(self, name):
        results = self.db.select_records_where_condition(
            self.table_name, 'name', name)

        name_exists = False

        if len(results) > 0:
            name_exists = True

        return name_exists

    def prefix_exists(self, prefix):
        results = self.db.select_records_where_condition(
            self.table_name, 'prefix', prefix)

        prefix_exists = False

        if len(results) > 0:
            prefix_exists = True

        return prefix_exists

    def select_all(self):
        results = self.db.select_all_records(self.table_name)

        warehouses = []

        for res in results:
            warehouse = self.result_to_model(res)
            warehouses.append(warehouse)

        return warehouses

    def select_by_id(self, warehouse_id):
        result = self.db.select_record_by_id(self.table_name, warehouse_id)
        warehouse = self.result_to_model(result)

        return warehouse

    def select_all_as_dicts(self):
        warehouses = self.select_all()
        warehouse_dicts = []

        for w in warehouses:
            w_dict = {
                'id': w.id,
                'name': w.name,
                'description': w.description,
                'notes': w.notes,
                'prefix': w.prefix,
                'street_address': w.street_address, 'city': w.city, 'postal_code': w.postal_code, 'province': w.province, 'country': w.country,
                'manager_name': w.manager_name, 'manager_email': w.manager_email, 'manager_phone': w.manager_phone,
                'created_at': w.created_at
            }
            warehouse_dicts.append(w_dict)

        return warehouse_dicts

    def create(self, form_data):
        # Array for errors
        errors = []

        # Check if name already exists
        name = form_data.get('name')
        if self.name_exists(name) is True:
            error = 'The name provided is already being used by another record in the database!'
            errors.append(error)

        # Check if prefix already exists
        prefix = form_data.get('prefix')
        if self.prefix_exists(prefix) is True:
            error = 'The prefix provided is already being used by another record in the database!'
            errors.append(error)

        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.insert_single_record(self.table_name, self.columns, values)

        return errors

    def delete(self, warehouse_id):
        self.db.delete_single_record(self.table_name, warehouse_id)

    def edit(self, form_data, warehouse_id):
        errors = []

        # TODO: Form validation

        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.update_single_record(self.table_name, self.columns, values, warehouse_id)
        
        return errors

    def details(self, warehouse_id):
        result = self.db.select_record_by_id(self.table_name, warehouse_id)
        warehouse = self.result_to_model(result)

        return warehouse
