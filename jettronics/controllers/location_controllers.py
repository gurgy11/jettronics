from jettronics.db import Database
from jettronics.models import Location
from . import Controller


class LocationsController(Controller):

    def __init__(self):
        self.db = Database()
        self.table_name = 'locations'
        self.columns = ['name', 'description', 'notes', 'warehouse_prefix',
                        'available', 'product_sku', 'product_quantity']

    def form_data_to_model(self, form_data):
        # Form data values
        name = form_data.get('name')
        description = form_data.get('description')
        notes = form_data.get('notes')
        warehouse_prefix = form_data.get('warehouse_prefix')
        available = form_data.get('available')
        product_sku = form_data.get('product_sku')
        product_quantity = form_data.get('product_quantity')

        # Model
        location = Location(name, description, notes, warehouse_prefix,
                            available, product_sku, product_quantity)

        return location

    def result_to_model(self, result):
        # Result data values
        lid = result[0]
        name = result[1]
        description = result[2]
        notes = result[3]
        warehouse_prefix = result[4]
        available = result[5]
        product_sku = result[6]
        product_quantity = result[7]
        created_at = result[8]

        # Model
        location = Location(name, description, notes, warehouse_prefix,
                            available, product_sku, product_quantity)
        location.id = lid
        location.created_at = created_at

        return location

    def form_data_to_values(self, form_data):
        location = self.form_data_to_model(form_data)
        values = (location.name, location.description, location.notes, location.warehouse_prefix,
                  location.available, location.product_sku, location.product_quantity)

        return values

    def name_exists(self, name):
        name_exists = False

        results = self.db.select_records_where_condition(
            self.table_name, 'name', name)

        if len(results) > 0:
            name_exists = True

        return name_exists

    def select_all(self):
        results = self.db.select_all_records(self.table_name)

        locations = []

        for res in results:
            location = self.result_to_model(res)
            locations.append(location)

        return locations

    def select_by_id(self, location_id):
        result = self.db.select_record_by_id(location_id)
        location = self.result_to_model(result)

        return location

    def create(self, form_data):
        errors = []

        # Validate form
        if self.name_exists(form_data.get('name')) is True:
            error = 'The name provided is already being used by another location in the database!'
            errors.append(error)
        if form_data.get('warehouse_prefix') is None or form_data.get('warehouse_prefix') == '':
            error = 'The warehouse prefix field cannot be empty. Please provide a value!'
            errors.append(error)

        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.insert_single_record(self.table_name, self.columns, values)

        return errors

    def delete(self, location_id):
        self.db.delete_single_record(self.table_name, location_id)

    def edit(self, form_data, location_id):
        errors = []

        # TODO: Form validation

        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.update_single_record(self.table_name, self.columns, values, location_id)

        return errors

    def details(self, location_id):
        result = self.db.select_record_by_id(self.table_name, location_id)
        location = self.result_to_model(result)

        return location