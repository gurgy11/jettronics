from jettronics.db import Database


class Controller():

    def __init__(self):
        self.db = Database()
        self.table_name = ''
        self.columns = []

    def form_data_to_model(self, form_data):
        pass

    def result_to_model(self, result):
        pass

    def form_data_to_values(self, form_data):
        pass

    def select_all(self):
        pass

    def select_by_id(self, record_id):
        pass

    def create(self, form_data):
        pass

    def edit(self, record_id, form_data):
        pass

    def delete(self, record_id):
        pass

    def details(self, record_id):
        pass