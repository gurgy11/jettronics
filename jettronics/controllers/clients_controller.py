from jettronics.controllers import Controller
from jettronics.db import Database
from jettronics.models import Client
from jettronics.utils import Paginator


class ClientsController(Controller):
    
    ### Constructor ###
    def __init__(self):
        self.db = Database()
        set.table_name = 'clients'
        self.columns = [
            'name',
            'street_address',
            'city',
            'postal_code',
            'province',
            'country',
            'notes',
            'last_ordered_at'
        ]
        self.paginator = Paginator(module_name='clients')
    
    ### Conversion Methods ###
    
    def result_to_model(self, result):
        pass
    
    def results_to_model(self, results):
        pass
    
    def result_to_dict(self, result):
        pass
    
    def results_to_dict(self, results):
        pass
    
    def form_data_to_model(self, form_data):
        pass
    
    def form_data_to_dict(self, form_data):
        pass
    
    def form_data_to_values(self, form_data):
        pass
    
    def model_to_dict(self, client):
        pass
    
    def models_to_dict(self, clients):
        pass
    
    def select_all(self):
        pass
    
    def select_all_as_dict(self):
        pass
    
    def select_by_id(self, record_id):
        pass
    
    def select_by_id_as_dict(self, record_id):
        pass
    
    def select_in_range(self, page_number, limit_per_page):
        pass
    
    def select_in_range_as_dict(self, page_number, limit_per_page):
        pass
    
    def name_exists(self, name):
        pass
    
    def create(self, form_data):
        pass
    
    def edit(self, form_data, client_id):
        pass
    
    def delete(self, client_id):
        pass