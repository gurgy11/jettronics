from jettronics.db import Database
from jettronics.models import Category
from .controller import Controller
from jettronics.utils import Paginator
import math

class CategoriesController(Controller):

    def __init__(self):
        self.db = Database()
        self.table_name = 'categories'
        self.columns = ['name', 'description', 'notes']
        self.paginator = Paginator()

    def form_data_to_model(self, form_data):
        # Form data values
        name = form_data.get('name')
        description = form_data.get('description')
        notes = form_data.get('notes')

        # Category model
        category = Category(name, description, notes)

        return category

    def form_data_to_values(self, form_data):
        # Form data to model
        category = self.form_data_to_model(form_data)

        # Values tuple
        values = (category.name, category.description, category.notes)

        return values

    def result_to_model(self, result):
        # Result data values
        cid = result[0]
        name = result[1]
        description = result[2]
        notes = result[3]
        created_at = result[4]

        # Category model
        category = Category(name, description, notes)
        category.id = cid
        category.created_at = created_at

        return category
    
    def to_dict(self, category):
        category_dict = {
            'id': category.id,
            'name': category.name,
            'description': category.description,
            'notes': category.notes,
            'created_at': category.created_at
        }
        return category_dict

    # Checks if the category name exists in the database and returns a boolean
    def category_name_exists(self, category_name):
        results = self.db.select_records_where_condition(self.table_name, 'name', category_name)

        category_name_exists = False

        if len(results) > 0:
            category_name_exists = True
        
        return category_name_exists

    def select_all(self):
        # Fetch all records from database
        results = self.db.select_all_records(self.table_name)

        # Create an array of categories
        categories = []
        for res in results:
            category = self.result_to_model(res)
            categories.append(category)

        return categories

    def select_by_id(self, record_id):
        # Fetch single record from database
        result = self.db.select_record_by_id(self.table_name, record_id)

        # Convert to model
        category = self.result_to_model(result)

        return category
    
    def select_by_id_as_dict(self, record_id):
        result = self.db.select_record_by_id(self.table_name, record_id)
        
        category = self.result_to_model(result)
        
        category_dict = {
            'id': category.id,
            'name': category.name,
            'description': category.description,
            'notes': category.notes,
            'created_at': category.created_at
        }
        
        return category_dict

    def create(self, form_data):
        # Form data to model
        category = self.form_data_to_model(form_data)

        errors = []

        # Check if name already exists
        if self.category_name_exists(category.name) is True:
            error = 'The category name provided already exists in the database!'
            errors.append(error)

        # Create category if there are no errors
        if len(errors) <= 0:
            values = self.form_data_to_values(form_data)
            self.db.insert_single_record(self.table_name, self.columns, values)

        return errors

    def select_all_as_dict(self):
        categories = self.select_all()

        category_dicts = []

        for category in categories:
            category_dict = {
                'id': category.id,
                'name': category.name,
                'description': category.description,
                'notes': category.notes,
                'created_at': category.created_at
            }
            category_dicts.append(category_dict)

        return category_dicts

    def edit(self, category_id, form_data):
        # Convert form to values
        values = self.form_data_to_values(form_data)

        print(values)

        # Update in database
        self.db.update_single_record(self.table_name, self.columns, values, category_id)

    def delete(self, category_id):
        # Delete in database
        cursor = self.db.cursor()
        
        query_str = "DELETE FROM categories WHERE id=" + category_id
        
        cursor.execute(query_str)
        
        conn = self.db.connection
        conn.commit()
        
    def select_in_range(self, start_index, end_index):
        all_categories = self.select_all()
        
        start_index = int(start_index)
        end_index = int(end_index)
        
        categories = []
        count = start_index
        while count < end_index and count < len(all_categories):
            category = all_categories[count]
            
            category_dict = self.to_dict(category)
            categories.append(category_dict)

            count = count + 1
            
        return categories
    
    def number_of_categories(self):
        categories = self.select_all()
        return len(categories)
    
    def get_number_of_pages(self, limit):
        category_count = self.number_of_categories()
        page_count = math.ceil(category_count / limit)
        return page_count
    
    def get_pages(self, limit):
        limit = int(limit)
        page = 1
        
        number_of_categories = self.number_of_categories()
        number_of_pages = self.paginator.get_number_of_pages(limit, number_of_categories)
        
        pages = []
        
        for i in range(number_of_pages):
            start_index = 0
            end_index = 10
            
            if limit == 10:
                if page == 1:
                    start_index = 0
                    end_index = 10
                else:
                    start_index = (page * 10) - 10
                    end_index = start_index + 10
            elif limit == 20:
                if page == 1:
                    start_index = 0
                    end_index = 20
                elif page % 2 == 0:
                    start_index = page * 10
                    end_index = start_index + 20
                else:
                    start_index = (page * 10) + 10
                    end_index = start_index + 20
            
            categories = self.select_in_range(start_index, end_index)
            
            page_data = {
                'page_count': number_of_pages,
                'start_index': start_index,
                'end_index': end_index,
                'categories': categories
            }
            pages.append(page_data)
            
            page = page + 1
        
        return pages