import pandas as pd
import os
import math
from flask import Blueprint, request, render_template, redirect, url_for, session, jsonify, current_app, send_from_directory, send_file
from jettronics.controllers import CategoriesController
from jettronics.login_manager import LoginManager
from jettronics.utils import Paginator

# Blueprint
bp = Blueprint('categories', __name__)

# Controller
controller = CategoriesController()

# Login Manager
lman = LoginManager()


# Index
@bp.route('/categories')
@bp.route('/categories/index')
def index():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # categories = controller.select_in_range(page, limit)
    
    return render_template('categories/index.html', title='Categories - Index')


# Create
@bp.route('/categories/create', methods=['GET', 'POST'])
def create():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        description = request.form.get('description')
        notes = request.form.get('notes')

        # Form data
        form_data = {
            'name': name, 'description': description, 'notes': notes
        }

        errors = controller.create(form_data)

        if len(errors) > 0:
            return render_template('categories/create.html', title='Categories - Create', errors=errors)
        else:
            return redirect(url_for('categories.index'))

    return render_template('categories/create.html', title='Categories - Create')


# Delete
@bp.route('/categories/delete/<category_id>')
def delete(category_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Category ID
    cid = category_id

    # Delete in database
    controller.delete(category_id)

    return redirect(url_for('categories.index'))


# Edit
@bp.route('/categories/edit/<category_id>', methods=['GET', 'POST'])
def edit(category_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Category ID
    cid = category_id

    # Fetch category by ID
    category = controller.select_by_id(cid)

    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        description = request.form.get('description')
        notes = request.form.get('notes')

        # Form data
        form_data = {
            'name': name,
            'description': description,
            'notes': notes
        }

        # Edit in database
        controller.edit(cid, form_data)

        return redirect(url_for('categories.index'))

    return render_template('categories/edit.html', title='Categories - Edit', category=category)


# Details
@bp.route('/categories/details/<category_id>')
def details(category_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Category ID
    cid = category_id

    # Fetch category from database
    category = controller.select_by_id(cid)

    return render_template('categories/details.html', title='Categories - Details', category=category)


#### Export Routes ####


# Export to XLSX
@bp.route('/categories/export/excel')
def export_to_excel():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    # Categories array as dicts
    categories = controller.select_all_as_dict()

    # Create a dataframe from the array
    categories_df = pd.DataFrame(categories)

    # Download directory
    downloads = os.path.join(current_app.root_path, 'downloads/excel')

    # File name
    file_name = 'jettronics-categories.xlsx'

    # File path
    file_path = os.path.join(downloads, file_name)

    # Create .xlsx file from dataframe
    categories_df.to_excel(file_path)

    return send_file(file_path,
                     mimetype='text/xlsx',
                     attachment_filename=file_name,
                     as_attachment=True)


# Export to CSV
@bp.route('/categories/export/csv')
def export_to_csv():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    # Categories array as dicts
    categories = controller.select_all_as_dict()

    # DataFrame
    categories_df = pd.DataFrame(categories)

    # Downloads directory
    downloads = os.path.join(current_app.root_path, 'downloads/csv')

    # File name
    file_name = 'jettronics-categories.csv'

    # File path
    file_path = os.path.join(downloads, file_name)

    # Create .csv file from DataFrame
    categories_df.to_csv(file_path)

    return send_file(file_path,
                     mimetype='text/csv',
                     attachment_filename=file_name,
                     as_attachment=True)


# Export to JSON
@bp.route('/categories/export/json')
def export_to_json():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    # Categories array as dicts
    categories = controller.select_all_as_dict()

    # DataFrame
    categories_df = pd.DataFrame(categories)

    # Downloads directory
    downloads = os.path.join(current_app.root_path, 'downloads/json')

    # File name
    file_name = 'jettronics-categories.json'

    # File path
    file_path = os.path.join(downloads, file_name)

    # Create .json
    categories_df.to_json(file_path)

    return send_file(file_path,
                     mimetype='text/json',
                     attachment_filename=file_name,
                     as_attachment=True)


#### API Routes ####


# Fetches all category records
@bp.route('/categories/fetch-all')
def fetch_all():
    
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    categories = controller.select_all_as_dict()
    
    return jsonify(categories)


# Fetches a single category by its ID
@bp.route('/categories/fetch-by-id/<category_id>')
def fetch_by_id(category_id):
    
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    category = controller.select_by_id_as_dict(category_id)
    
    return jsonify(category)


# Fetches categories with a start and end index
@bp.route('/categories/fetch-category-pages/<limit>')
def fetch_pages(limit):
    # categories = controller.select_in_range(start_index, end_index)
    
    pages = controller.get_pages(limit)
    
    return jsonify(pages)


@bp.route('/categories/fetch-in-range/<page>/<limit>')
def fetch_in_range(page, limit):
    page = int(page)
    limit = int(limit)
    
    pages = controller.get_pages(limit)
    
    page_index = page - 1
    page_data = None
    
    try:
        page_data = pages[page_index]
    except:
        pass
    
    return jsonify(page_data)


@bp.route('/categories/fetch-page-count/<limit>')
def fetch_page_count(limit):
    categories = controller.select_all()
    page_count = math.ceil(len(categories) / limit)