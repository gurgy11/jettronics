import os
import pandas as pd

from flask import Blueprint, request, render_template, redirect, url_for, jsonify, send_file, current_app

from jettronics.controllers import LocationsController
from jettronics.login_manager import LoginManager

# Blueprint
bp = Blueprint('locations', __name__)

# Controller
controller = LocationsController()

# Login Manager
lman = LoginManager()


### Template Rendering Routes ###


# Index
@bp.route('/locations')
@bp.route('/locations/index')
def index():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    locations = controller.select_all()

    return render_template('locations/index.html', title='Locations - Index', locations=locations)


# Create
@bp.route('/locations/create', methods=['GET', 'POST'])
def create():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # POST
    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        description = request.form.get('description')
        notes = request.form.get('notes')
        warehouse_prefix = request.form.get('warehouse_prefix')
        available = request.form.get('available')
        product_sku = request.form.get('product_sku')
        product_quantity = request.form.get('product_quantity')

        # Form data
        form_data = {
            'name': name,
            'description': description,
            'notes': notes,
            'warehouse_prefix': warehouse_prefix,
            'available': available,
            'product_sku': product_sku, 'product_quantity': product_quantity
        }

        errors = controller.create(form_data)

        if len(errors) > 0:
            return render_template('locations/create.html', title='Locations - Create', errors=errors)
        else:
            return redirect(url_for('locations.index'))

    return render_template('locations/create.html', title='Locations - Create')


# Delete
@bp.route('/locations/delete/<location_id>')
def delete(location_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Location ID
    lid = location_id

    controller.delete(lid)

    return redirect(url_for('locations.index'))


# Edit
@bp.route('/locations/edit/<location_id>', methods=['GET', 'POST'])
def edit(location_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Location ID
    lid = location_id

    if request.method == 'POST':

    location = controller.select_by_id(lid)
    return render_template('locations/edit.html', title='Locations - Edit', location=location)


# Details


### Export Routes ###


# Export to Excel


# Export to CSV


# Export to JSON


### API Routes ###


# Fetch All


# Fetch By ID