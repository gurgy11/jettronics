import os
import pandas as pd

from flask import Blueprint, request, render_template, redirect, url_for, jsonify, send_file, current_app

from jettronics.controllers import WarehousesController
from jettronics.login_manager import LoginManager

# Blueprint
bp = Blueprint('warehouses', __name__)

# Controller
controller = WarehousesController()

# Login Manager
lman = LoginManager()


### Template Rendering Routes ###


# Index
@bp.route('/warehouses')
@bp.route('/warehouses/index')
def index():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Pagination Page
    page = request.args.get('page', 1, type=int)

    warehouses = controller.select_all()

    return render_template('warehouses/index.html', title='Warehouses - Index', warehouses=warehouses)


# Create
@bp.route('/warehouses/create', methods=['GET', 'POST'])
def create():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # POST
    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        description = request.form.get('description')
        notes = request.form.get('notes')
        prefix = request.form.get('prefix')
        street_address = request.form.get('street_address')
        city = request.form.get('city')
        postal_code = request.form.get('postal_code')
        province = request.form.get('province')
        country = request.form.get('country')
        manager_name = request.form.get('manager_name')
        manager_email = request.form.get('manager_email')
        manager_phone = request.form.get('manager_phone')

        # Form data
        form_data = {
            'name': name,
            'description': description,
            'notes': notes,
            'prefix': prefix,
            'street_address': street_address, 'city': city, 'postal_code': postal_code, 'province': province, 'country': country,
            'manager_name': manager_name, 'manager_email': manager_email, 'manager_phone': manager_phone
        }

        errors = controller.create(form_data)

        if len(errors) > 0:
            return render_template('warehouses/create.html', title='Warehouses - Create', errors=errors)
        else:
            return redirect(url_for('warehouses.index'))

    return render_template('warehouses/create.html', title='Warehouses - Create')


# Delete
@bp.route('/warehouses/delete/<warehouse_id>')
def delete(warehouse_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Warehouse ID
    wid = warehouse_id

    # Delete from db
    controller.delete(wid)

    return redirect(url_for('warehouses.index'))


# Edit
@bp.route('/warehouses/edit/<warehouse_id>', methods=['GET', 'POST'])
def edit(warehouse_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Warehouse
    wid = warehouse_id
    warehouse = controller.select_by_id(wid)

    # POST
    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        description = request.form.get('description')
        notes = request.form.get('notes')
        prefix = request.form.get('prefix')
        street_address = request.form.get('street_address')
        city = request.form.get('city')
        postal_code = request.form.get('postal_code')
        province = request.form.get('province')
        country = request.form.get('country')
        manager_name = request.form.get('manager_name')
        manager_email = request.form.get('manager_email')
        manager_phone = request.form.get('manager_phone')

        # Form data
        form_data = {
            'name': name,
            'description': description,
            'notes': notes,
            'prefix': prefix,
            'street_address': street_address, 'city': city, 'postal_code': postal_code, 'province': province, 'country': country,
            'manager_name': manager_name, 'manager_email': manager_email, 'manager_phone': manager_phone
        }

        errors = controller.edit(form_data, wid)

        if len(errors) > 0:
            return render_template('warehouses/edit.html', title='Warehouses - Edit', errors=errors)
        else:
            return redirect(url_for('warehouses.index'))

    return render_template('warehouses/edit.html', title='Warehouses - Edit', warehouse=warehouse)


# Details
@bp.route('/warehouses/details/<warehouse_id>')
def details(warehouse_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))

    # Warehouse id
    wid = warehouse_id

    warehouse = controller.select_by_id(warehouse_id)

    return render_template('warehouses/details.html', title='Warehouses - Details', warehouse=warehouse)


### Export Routes ###


# Export to Excel
@bp.route('/warehouses/export/excel')
def export_to_excel():
    warehouse_dicts = controller.select_all_as_dicts()

    # Pandas DataFrame
    warehouses_df = pd.DataFrame(warehouse_dicts)

    # File parameters
    directory = os.path.join(current_app.root_path, 'downloads/excel')
    name = 'jettronics-warehouses.xlsx'
    path = os.path.join(directory, name)

    # Create xlsx file
    warehouses_df.to_excel(path)

    # Return xlsx file
    return send_file(path,
                     mimetype='text/xlsx',
                     attachment_filename=name,
                     as_attachment=True)


# Export to CSV
@bp.route('/warehouses/export/csv')
def export_to_csv():
    warehouse_dicts = controller.select_all_as_dicts()
    warehouses_df = pd.DataFrame(warehouse_dicts)

    directory = os.path.join(current_app.root_path, 'downloads/csv')
    name = 'jettronics-warehouses.csv'
    path = os.path.join(directory, name)

    warehouses_df.to_csv(path)

    return send_file(path, mimetype='text/csv', attachment_filename=name, as_attachment=True)


# Export to JSON
@bp.route('/warehouses/export/json')
def export_to_json():
    warehouse_dicts = controller.select_all_as_dicts()
    warehouses_df = pd.DataFrame(warehouse_dicts)

    directory = os.path.join(current_app.root_path, 'downloads/json')
    name = 'jettronics-warehouses.json'
    path = os.path.join(directory, name)

    warehouses_df.to_json(path)

    return send_file(path, mimetype='text/json', attachment_filename=name, as_attachment=True)


### API Routes ###


# Fetch All
@bp.route('/warehouses/api/fetch-all')
def fetch_all():
    warehouses = controller.select_all_as_dicts()
    return jsonify(warehouses)
