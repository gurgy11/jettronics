import os
import pandas as pd
from flask import (Blueprint, request, render_template, redirect, url_for, 
                   jsonify, current_app, send_file)
from jettronics.controllers import ProductsController
from jettronics.login_manager import LoginManager
from jettronics.controllers import CategoriesController

bp = Blueprint('products', __name__)
controller = ProductsController()
categories_controller = CategoriesController()
lman = LoginManager()


### Template Rendering Routes ###


# Index
@bp.route('/products')
@bp.route('/products/index')
def index():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    return render_template('products/index.html', title='Products - Index')


# Create
@bp.route('/products/create', methods=['GET', 'POST'])
def create():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        category_name = request.form.get('category_name')
        description = request.form.get('description')
        warehouse_prefix = request.form.get('warehouse_prefix')
        location = request.form.get('location')
        price_per_unit = request.form.get('price_per_unit')
        quantity = request.form.get('quantity')
        sku = request.form.get('sku')
        barcode = request.form.get('barcode')
        country_of_origin = request.form.get('country_of_origin')
        notes = request.form.get('notes')
        
        # Form data
        form_data = {
            'name': name,
            'category_name': category_name,
            'description': description,
            'warehouse_prefix': warehouse_prefix, 'location': location,
            'price_per_unit': price_per_unit, 'quantity': quantity,
            'sku': sku, 'barcode': barcode,
            'country_of_origin': country_of_origin,
            'notes': notes,
        }
        
        errors = controller.create(form_data)
        
        if len(errors) <= 0:
            return redirect(url_for('products.index'))
        else:
            return render_template('products/create.html', title='Products - Create', errors=errors)
    
    return render_template('products/create.html', title='Products - Create')


# Delete
@bp.route('/products/delete/<product_id>')
def delete(product_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    pid = product_id
    controller.delete(pid)
    
    return redirect(url_for('product.index'))


# Edit
@bp.route('/products/edit/<product_id>', methods=['GET', 'POST'])
def edit(product_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    pid = product_id
    
    if request.method == 'POST':
        # Request form data
        name = request.form.get('name')
        category_name = request.form.get('category_name')
        description = request.form.get('description')
        warehouse_prefix = request.form.get('warehouse_prefix')
        location = request.form.get('location')
        price_per_unit = request.form.get('price_per_unit')
        quantity = request.form.get('quantity')
        sku = request.form.get('sku')
        barcode = request.form.get('barcode')
        country_of_origin = request.form.get('country_of_origin')
        notes = request.form.get('notes')
        
        # Form data
        form_data = {
            'name': name,
            'category_name': category_name,
            'description': description,
            'warehouse_prefix': warehouse_prefix, 'location': location,
            'price_per_unit': price_per_unit, 'quantity': quantity,
            'sku': sku, 'barcode': barcode,
            'country_of_origin': country_of_origin,
            'notes': notes,
        }
        
        errors = controller.edit(pid, form_data)
        
        if len(errors) <= 0:
            return redirect(url_for('products.index'))
        else:
            return render_template('products/edit.html', title='Products - Edit', errors=errors)
    
    product = controller.select_by_id(pid)
    return render_template('products/edit.html', title='Products - Edit', product=product)


# Details
@bp.route('/products/details/<product_id>')
def details(product_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    pid = product_id
    product = controller.select_by_id(pid)
    
    return render_template('products/details.html', title='Products - Details', product=product)


### Export Routes ###


# Export to Excel
@bp.route('/products/export/excel')
def export_to_excel():
    products = controller.select_all_as_dict()
    products_df = pd.DataFrame(products)
    
    ROOT_DIR = current_app.root_path
    downloads_dir = os.path.join(ROOT_DIR, 'downloads/excel')
    
    file_name = 'jettronics-products.xlsx'
    file_path = os.path.join(downloads_dir, file_name)
    
    products_df.to_excel(file_path)
    
    return send_file(file_path, as_attachment=True)


# Export to CSV
@bp.route('/products/export/csv')
def export_to_csv():
    products = controller.select_all_as_dict()
    products_df = pd.DataFrame(products)
    
    ROOT_DIR = current_app.root_path
    downloads_dir = os.path.join(ROOT_DIR, 'downloads/csv')
    file_name = 'jettronics-products.csv'
    
    file_path = os.path.join(downloads_dir, file_name)
    
    products_df.to_csv(file_path)
    
    return send_file(file_path, as_attachment=True)


# Export to JSON
@bp.route('/products/export/json')
def export_to_json():
    products = controller.select_all_as_dict()
    products_df = pd.DataFrame(products)
    
    root_dir = current_app.root_path
    downloads_dir = os.path.join(root_dir, 'downloads/json')
    file_name = 'jettronics-products.json'
    
    file_path = os.path.join(downloads_dir, file_name)
    
    products_df.to_json(file_path)
    
    return send_file(file_path, as_attachment=True)


### API Routes ###


# Fetch All
@bp.route('/products/fetch-all')
def fetch_all():
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    products = controller.select_all_as_dicts()
    
    return jsonify(products)


# Fetch By ID
@bp.route('/products/fetch-by-id/<product_id>')
def fetch_by_id(product_id):
    # Authenticate
    if lman.user_is_logged_in() is not True:
        return redirect(url_for('auth.login_required'))
    
    product = controller.select_by_id_as_dict(product_id)
    
    return jsonify(product)


# Fetch In Range
@bp.route('/products/fetch-in-range/<page>/<limit>')
def fetch_in_range(page, limit):
    page_number = int(page)
    limit_per_page = int(limit)
    
    products = controller.select_in_range(page_number, limit_per_page)
    
    return jsonify(products)


# Fetch Page Count
@bp.route('/products/fetch-page-count/<limit>')
def fetch_page_count(limit):
    page_count = controller.get_number_of_pages(int(limit))
    res = {
        'page_count': page_count
    }
    return jsonify(res)