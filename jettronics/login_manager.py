from flask import current_app, session


class LoginManager():

    def __init__(self):
        self.app = current_app

    def user_is_logged_in(self):
        user_is_logged_in = False
        with self.app.app_context():
            if 'user_id' in session:
                user_is_logged_in = True
        return user_is_logged_in

    def add_user_to_session(self, uid, username):
        user_id = uid
        user_username = username

        with self.app.app_context():
            session['user_id'] = user_id
            session['user_username'] = user_username