import os
from flask import Flask, render_template, session

def create_app(test_config=None):

    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY='dev'
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    @app.route('/index')
    def index():
        return render_template('index.html', title='JetTronics - Home')
    
    ### TEMPLATE RENDERING ROUTES ###

    # AUTH
    from . import auth
    app.register_blueprint(auth.bp)
    app.add_url_rule('/', endpoint='auth')

    # CATEGORIES
    from .views.categories import categories
    app.register_blueprint(categories.bp)
    app.add_url_rule('/', endpoint='categories')

    # WAREHOUSES
    from .views.storages import warehouses
    app.register_blueprint(warehouses.bp)
    app.add_url_rule('/', endpoint='warehouses')
    
    # LOCATIONS
    
    # PRODUCTS
    from .views.products import products
    app.register_blueprint(products.bp)
    app.add_url_rule('/', endpoint='products')

    return app