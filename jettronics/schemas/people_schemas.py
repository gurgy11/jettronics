'''
People Schemas:
    - PersonSchema
    - ContactSchema
    - ClientContactSchema
    - SupplierContactSchema
    - EmployeeSchema
    - UserSchema
'''

from marshmallow import fields, Schema


# Person Schema
class PersonSchema(Schema):
    # Fields
    pid = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    gender = fields.Str()
    date_of_birth = fields.Date()
    ethnicity = fields.Str()
    email_address = fields.Email()
    phone_number = fields.Str()
    street_address = fields.Str()
    city = fields.Str()
    postal_code = fields.Str()
    province = fields.Str()
    country = fields.Str()
    notes = fields.Text()
    created_at = fields.DateTime()


# Contact Schema
class ContactSchema(PersonSchema):
    # Fields
    contact_type = fields.Str(required=True)


# Client Contact Schema
class ClientContactSchema(ContactSchema):
    # Fields
    client_name = fields.Str(required=True)


# Supplier Contact Schema
class SupplierContactSchema(ContactSchema):
    # Fields
    supplier_name = fields.Str(required=True)


# Employee Schema 
class EmployeeSchema(PersonSchema):
    # Fields
    job_title = fields.Str(required=True)
    warehouse_name = fields.Str()
    wage_unit = fields.Str(required=True)
    wage_per_unit = fields.Decimal(required=True)
    employed_at = fields.DateTime()


# User Schema
class UserSchema(EmployeeSchema):
    # Fields
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    account_role = fields.Str(required=True)
    last_login_at = fields.DateTime()
