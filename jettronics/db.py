from mysql.connector import connect
from jettronics.config import db_credentials


class Database():

    def __init__(self):
        self.host       = db_credentials.get('host')
        self.user       = db_credentials.get('user')
        self.password   = db_credentials.get('password')
        self.database   = db_credentials.get('database')
        self.connection = connect(
            host=self.host,
            user=self.user,
            password=self.password,
            database=self.database
        )

    def cursor(self):
        return self.connection.cursor()

    def select_all_records(self, table_name):
        query = """SELECT * FROM {table_name}""".format(table_name=table_name)

        cursor = self.cursor()
        cursor.execute(query)

        results = cursor.fetchall()
        data = []
        for res in results:
            data.append(res)

        return data

    def select_record_by_id(self, table_name, record_id):
        query = """SELECT * FROM {table_name} WHERE id={record_id}""".format(table_name=table_name, record_id=record_id)

        cursor = self.cursor()
        cursor.execute(query)

        results = cursor.fetchall()
        result = results[0]

        return result

    def select_records_where_condition(self, table_name, column_name, column_value):
        query = """SELECT * FROM {table_name} WHERE {column_name}="{column_value}" """.format(
            table_name=table_name,
            column_name=column_name,
            column_value=column_value
        )

        cursor = self.cursor()
        cursor.execute(query)

        results = cursor.fetchall()

        data = []
        for res in results:
            data.append(res)

        return data

    def insert_single_record(self, table_name, columns, values):
        query_str = """INSERT INTO {table_name} (""".format(table_name=table_name)

        for col in columns:
            col_str = """{column_name}""".format(column_name=col)
            if columns.index(col) == len(columns) - 1:
                col_str += ") "
            else:
                col_str += ", "
            query_str += col_str

        query_str += "VALUES ("

        for val in values:
            val_str = "%s"
            if values.index(val) == len(values) - 1:
                val_str += ") "
            else:
                val_str += ", "
            query_str += val_str

        print(query_str)

        cursor = self.cursor()
        cursor.execute(query_str, values)

        conn = self.connection
        conn.commit()

    def update_single_record(self, table_name, columns, values, record_id):
        query_str = """UPDATE {table_name} SET """.format(table_name=table_name)

        for col in columns:
            col_str = """{column_name} = %s""".format(column_name=col)
            if columns.index(col) == len(columns) - 1:
                col_str += " "
            else:
                col_str += ", "
            query_str += col_str

        query_str += """WHERE id={id}""".format(id=record_id)

        print(query_str)

        cursor = self.cursor()
        cursor.execute(query_str, values)

        conn = self.connection
        conn.commit()

    def delete_single_record(self, table_name, record_id):
        query_str = "DELETE FROM " + table_name + " WHERE id=" + record_id

        cursor = self.cursor()
        cursor.execute(query_str)

        conn = self.connection
        conn.commit()
        
    def select_query(self, query_str):
        cursor = self.cursor()
        cursor.execute(query_str)
        
        results = cursor.fetchall()
        
        data = []
        for res in results:
            data.append(res)
            
        return data