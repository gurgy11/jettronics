import math


class Paginator():
    
    # Constructor
    def __init__(self, module_name=None):
        self._module_name = module_name
        
    # Module Name
    @property
    def module_name(self):
        return self._module_name
    
    @module_name.setter
    def module_name(self, module_name):
        self._module_name = module_name
        
    # Gets the length of an array from the database
    def get_number_of_items(self, arr):
        number_of_items = len(arr)
        return number_of_items
    
    # Calculates and returns the number of pages
    def get_number_of_pages(self, number_of_items, limit_per_page):
        page_count = math.ceil(int(number_of_items) / int(limit_per_page))
        return page_count
    
    # Creates an array of pages with their start and end indexes
    def get_page_index_ranges(self, number_of_pages, limit_per_page):
        pages_count = int(number_of_pages)
        limit = int(limit_per_page)
        
        pages = []
        page = 1
        
        for i in range(pages_count):
            start_index = 0
            end_index = 10
            
            if limit == 10:
                if page == 1:
                    start_index = 0
                    end_index = 10
                else:
                    start_index = (page * 10) - 10
                    end_index = start_index + 10
            elif limit == 20:
                if page == 1:
                    start_index = 0
                    end_index = 20
                elif page % 2 == 0:
                    start_index = page * 10
                    end_index = start_index + 20
                else:
                    start_index = (page * 10) + 10
                    end_index = start_index + 20
            
            page_range = {
                'start_index': start_index,
                'end_index': end_index
            }
            pages.append(page_range)
        
        return pages