$(document).ready(function() {

    // Fetch All Categories
    $.ajax({
        type: 'GET',
        url: '/categories/fetch-all',
        dataType: 'JSON',
        success: function(res) {
            $.each(res, function(key, value) {
                var categoryName = value.name;
                var categoryOption = $('<option value="' + categoryName + '">' + categoryName + '</option>');
                $('#categoryName').append(categoryOption);
            });
        },
        error: function(err) {
            console.error(err);
        }
    });

});