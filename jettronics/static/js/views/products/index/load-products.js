$(document).ready(function() {

    var products = Array();

    $('#itemsPerPage').on('change', function() {
        limit = $(this).val();
        page = 1;
        window.location.href = '/products/index?page=' + page + '&limit=' + limit;
    });

    // Load products from server
    $.ajax({
        type: 'GET',
        url: '/products/fetch-in-range/' + page + '/' + limit,
        success: function(res) {
            $.each(res, function(key, value) {

                var tr = $('<tr></tr>');

                var idTd = $('<td>' + value.id + '</td>');
                var nameTd = $('<td>' + value.name + '</td>');
                var categoryNameTd = $('<td>' + value.category_name + '</td>');
                var descriptionTd = $('<td>' + value.description + '</td>');
                var warehousePrefixTd = $('<td>' + value.warehouse_prefix + '</td>');
                var locationTd = $('<td>' + value.location + '</td>');
                var pricePerUnitTd = $('<td>' + value.price_per_unit + '</td>');
                var quantityTd = $('<td>' + value.quantity + '</td>');
                var skuTd = $('<td>' + value.sku + '</td>');
                var barcodeTd = $('<td>' + value.barcode + '</td>');
                var countryOfOriginTd = $('<td>' + value.country_of_origin + '</td>');
                var notesTd = $('<td>' + value.notes + '</td>');
                var createdAtTd = $('<td>' + value.created_at + '</td>');
                var optionsTd = $('<td></td>');

                var detailsBtn = $('<button type="button" class="btn btn-outline-primary form-control" value="' + value.id + '">Edit</button>');
                var editBtn = $('<button type="button" class="btn btn-outline-warning form-control" value="' + value.id + '">Edit</button>');
                var deleteBtn = $('<button type="button" class="btn btn-outline-danger form-control" value="' + value.id + '">Delete</button>');

                $(detailsBtn).on('click', function() {
                    window.location.href = '/products/details/' + $(this).val();
                });
                $(editBtn).on('click', function() {
                    window.location.href = '/products/edit/' + $(this).val();
                });
                $(deleteBtn).on('click', function() {
                    window.location.href = '/products/delete/' + $(this).val();
                });

                optionsTd.append(detailsBtn);
                optionsTd.append(editBtn);
                optionsTd.append(deleteBtn);

                tr.append(idTd);
                tr.append(nameTd);
                tr.append(categoryNameTd);
                tr.append(descriptionTd);
                tr.append(warehousePrefixTd);
                tr.append(locationTd);
                tr.append(pricePerUnitTd);
                tr.append(quantityTd);
                tr.append(skuTd);
                tr.append(barcodeTd);
                tr.append(countryOfOriginTd);
                tr.append(notesTd);
                tr.append(createdAtTd);
                tr.append(optionsTd);

                $('#productsTable tbody').append(tr);

            });
        },
        error: function(err) {
            console.error(err);
        }
    });

    getPageCount();
    createPaginationButtons();

    function getPageCount() {
        $.ajax({
            type: 'GET',
            url: '/products/fetch-page-count/' + limit,
            dataType: 'JSON',
            success: function(res) {
                pageCount = res.page_count;
            },
            error: function(err) {
                console.error(err);
            }
        });
    }

    function createPaginationButtons() {
        var count = 0;
        while (count < pageCount) {
            var currPage = count + 1;
            var btn = $('<button type="button" class="pag-btn btn btn-outline-primary" value="' +
                currPage + '">' + currPage + '</button>');

            $('#paginationContainer').append(btn);

            count = count + 1;
        }

        $('.pag-btn').on('click', function(e) {
            e.preventDefault();
            var nextPage = $(this).val();
            window.location.href = '/products/index?page=' + nextPage + '&limit=' + limit;
        });
    }

});