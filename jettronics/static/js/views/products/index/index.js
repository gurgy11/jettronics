$(document).ready(function() {

    // Show in console that the script was loaded successfully
    console.log('Script was loaded successfully.');

    // Create New
    $('#createBtn').on('click', function() {
        window.location.href = '/products/create';
    });

    // Export to Excel
    $('#excelBtn').on('click', function(e) {
        e.preventDefault();
        window.location.href = '/products/export/excel';
    });

    // Export to CSV
    $('#csvBtn').on('click', function(e) {
        e.preventDefault();
        window.location.href = '/products/export/csv';
    });

    // Export to JSON
    $('#jsonBtn').on('click', function(e) {
        e.preventDefault();
        window.location.href = '/products/export/json';
    });

    // Products array
    products = Array();

    // Categories array
    categories = Array();

});