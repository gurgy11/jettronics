$(document).ready(function() {

    var categories = Array();

    $('#itemsPerPage').on('change', function() {
        limit = $(this).val();
        page = 1;
        window.location.href = '/categories/index?page=' + page + '&limit=' + limit;
    });

    $.ajax({
        type: 'GET',
        url: '/categories/fetch-in-range/' + page + '/' + limit,
        success: function(res) {
            $.each(res.categories, function(key, value) {

                var id = value.id;
                var name = value.name;
                var description = value.description;
                var notes = value.notes;
                var createdAt = value.created_at;

                var tr = $('<tr></tr>');

                var idTd = $('<td>' + id + '</td>');
                var nameTd = $('<td>' + name + '</td>');
                var descriptionTd = $('<td>' + description + '</td>');
                var notesTd = $('<td>' + notes + '</td>');
                var createdAtTd = $('<td>' + createdAt + '</td>');
                var optionsTd = $('<td></td>');

                var detailsBtn = $('<button type="button" class="btn btn-outline-primary form-control" value="' +
                    id + '">Details</button>');
                var editBtn = $('<button type="button" class="btn btn-outline-warning form-control" value="' +
                    id + '">Edit</button>');
                var deleteBtn = $('<button type="button" class="btn btn-outline-danger form-control" value="' +
                    id + '">Delete</button>');

                $(detailsBtn).on('click', function() {
                    window.location.href = '/categories/details/' + $(this).val();
                });
                $(editBtn).on('click', function() {
                    window.location.href = '/categories/edit/' + $(this).val();
                });
                $(deleteBtn).on('click', function() {
                    window.location.href = '/categories/delete/' + $(this).val();
                });

                optionsTd.append(detailsBtn);
                optionsTd.append(editBtn);
                optionsTd.append(deleteBtn);

                tr.append(idTd);
                tr.append(nameTd);
                tr.append(descriptionTd);
                tr.append(notesTd);
                tr.append(createdAtTd);
                tr.append(optionsTd);

                $('#categoriesTable tbody').append(tr);
            });

            pageCount = res.page_count;

        },
        error: function(err) {
            console.error(err);
        }
    });

    createPaginationButtons(pageCount);

    function createPaginationButtons(numPages) {
        var count = 0;
        while (count < numPages) {
            var currPage = count + 1;
            var btn = $('<button type="button" class="pag-btn btn btn-outline-primary" value="' +
                currPage + '">' + currPage + '</button>');

            $('#paginationContainer').append(btn);

            count = count + 1;
        }

        $('.pag-btn').on('click', function(e) {
            e.preventDefault();
            var nextPage = $(this).val();
            window.location.href = '/categories/index?page=' + nextPage + '&limit=' + limit
        });
    }

});