$(document).ready(function() {

    var queryStr = window.location.search;
    var urlParams = null;

    try {

        urlParams = new URLSearchParams(queryStr);

    } catch (err) {

        console.error(err);

    }

    setPage();
    setLimit();

    function setPage() {

        if (urlParams.get('page') != null) {

            page = urlParams.get('page');

        } else {

            page = 1;

        }

    }

    function setLimit() {

        if (urlParams.get('limit') != null) {
            limit = urlParams.get('limit');
        } else {
            limit = 10;
        }

    }

});