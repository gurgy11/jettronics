$(document).ready(function() {
    console.info('The index.js script was loaded successfully.');

    // Excel Button
    $('#excelBtn').on('click', function(e) {
        e.preventDefault();

        window.location = '/categories/export/excel';
    });

    // CSV Button
    $('#csvBtn').on('click', function(e) {
        e.preventDefault();

        window.location = '/categories/export/csv';
    });

    // Categories array
    var categories = Array();

    // Page
    var page = 1

    // Items per page selector
    var limit = 10
    $('#numberOfPages').on('change', function() {
        limit = $('#numberOfPages').val();
    });

    // Fetch all categories from api route
    $.ajax({
        type: 'GET',
        url: 'http://127.0.0.1:5000/categories/api/fetch-all',
        dataType: 'JSON',
        success: function(res) {
            $.each(res, function(key, value) {
                var category = {
                    'id': value.id,
                    'name': value.name,
                    'description': value.description,
                    'notes': value.notes,
                    'created_at': value.created_at
                };
                categories.push(category);
            });
            loadTableRows(categories);
        },
        error: function(err) {
            console.error(err);
        }
    });

    function loadTableRows(categories) {
        $.each(categories, function(key, value) {
            // Category fields
            var id = value.id;
            var name = value.name;
            var description = value.description;
            var notes = value.notes;
            var createdAt = value.created_at;

            // Category row
            var tr = $('<tr></tr>');

            // Category data elements
            var idTd = $('<td>' + id + '</td>');
            var nameTd = $('<td>' + name + '</td>');
            var descriptionTd = $('<td>' + description + '</td>');
            var notesTd = $('<td>' + notes + '</td>');
            var createdAtTd = $('<td>' + createdAt + '</td>');

            // Option Buttons Data
            var optionsTd = $('<td></td>');

            // Options Buttons (Delete, Edit, and Details)
            var deleteBtn = $('<button type="button" class="btn btn-outline-danger form-control" value="' + id + '">Delete</button>');
            var editBtn = $('<button type="button" class="btn btn-outline-warning form-control" value="' + id + '">Edit</button>');
            var detailsBtn = $('<button type="button" class="btn btn-outline-primary form-control" value="' + id + '">Details</button>');

            // Option Button Events
            $(deleteBtn).on('click', function() {
                window.location.href = '/categories/delete/' + $(this).val();
            });
            $(editBtn).on('click', function() {
                window.location.href = '/categories/edit/' + $(this).val();
            });
            $(detailsBtn).on('click', function() {
                window.location.href = '/categories/details/' + $(this).val();
            });

            // Append buttons to option
            optionsTd.append(deleteBtn);
            optionsTd.append(editBtn);
            optionsTd.append(detailsBtn);

            // Append data to row
            tr.append(idTd);
            tr.append(nameTd);
            tr.append(descriptionTd);
            tr.append(notesTd);
            tr.append(createdAtTd);
            tr.append(optionsTd);

            // Append row to table body
            $('#categoriesTable tbody').append(tr);
        });
    }

});