$(document).ready(function() {

    // Confirm script loaded successfully
    console.log('Script loaded successfully.');

    // Initialize warehouses array
    var warehouses = Array();

    // Fetch all warehouses from database
    $.ajax({
        type: 'GET',
        url: 'http://127.0.0.1:5000/warehouses/api/fetch-all',
        dataType: 'JSON',
        success: function(res) {
            $.each(res, function(key, value) {
                var warehouse = {
                    'id': value.id,
                    'name': value.name,
                    'description': value.description,
                    'notes': value.notes,
                    'prefix': value.prefix,
                    'street_address': value.street_address,
                    'city': value.city,
                    'postal_code': value.postal_code,
                    'province': value.province,
                    'country': value.country,
                    'manager_name': value.manager_name,
                    'manager_email': value.manager_email,
                    'manager_phone': value.manager_phone,
                    'created_at': value.created_at
                };
                warehouses.push(warehouse);
            });
            loadTableRows(warehouses);
        },
        error: function(err) {
            console.error(err);
        }
    });

    function loadTableRows(warehouses) {
        $.each(warehouses, function(key, value) {
            // Warehouse fields
            var id = value.id;
            var name = value.name;
            var description = value.description;
            var notes = value.notes;
            var prefix = value.prefix;
            var streetAddress = value.street_address;
            var city = value.city;
            var postalCode = value.postal_code;
            var province = value.province;
            var country = value.country;
            var managerName = value.manager_name;
            var managerEmail = value.manager_email;
            var managerPhone = value.manager_phone;
            var createdAt = value.created_at;

            // Table row
            var tr = $('<tr></tr>');

            // Table data elements
            var idTd = $('<td>' + id + '</td>');
            var nameTd = $('<td>' + name + '</td>');
            var descriptionTd = $('<td>' + description + '</td>');
            var notesTd = $('<td>' + notes + '</td>');
            var prefixTd = $('<td>' + prefix + '</td>');
            var streetAddressTd = $('<td>' + streetAddress + '</td>');
            var cityTd = $('<td>' + city + '</td>');
            var postalCodeTd = $('<td>' + postalCode + '</td>');
            var provinceTd = $('<td>' + province + '</td>');
            var countryTd = $('<td>' + country + '</td>');
            var managerNameTd = $('<td>' + managerName + '</td>');
            var managerEmailTd = $('<td>' + managerEmail + '</td>');
            var managerPhoneTd = $('<td>' + managerPhone + '</td>');
            var createdAtTd = $('<td>' + createdAt + '</td>');
            var optionsTd = $('<td></td>');

            // Options buttons
            var deleteBtn = $('<button type="button" class="btn btn-outline-danger form-control" value="' + id + '">Delete</button>');
            var editBtn = $('<button type="button" class="btn btn-outline-warning form-control" value="' + id + '">Edit</button>');
            var detailsBtn = $('<button type="button" class="btn btn-outline-primary form-control" value="' + id + '">Details</button>');

            // Button events
            $(deleteBtn).on('click', function() {
                window.location.href = '/warehouses/delete/' + $(this).val();
            });
            $(editBtn).on('click', function() {
                window.location.href = '/warehouses/edit/' + $(this).val();
            });
            $(detailsBtn).on('click', function() {
                window.location.href = '/warehouses/details/' + $(this).val();
            });

            // Append buttons to options
            optionsTd.append(deleteBtn);
            optionsTd.append(editBtn);
            optionsTd.append(detailsBtn);

            // Append data to row
            tr.append(idTd);
            tr.append(nameTd);
            tr.append(descriptionTd);
            tr.append(notesTd);
            tr.append(prefixTd);
            tr.append(streetAddressTd);
            tr.append(cityTd);
            tr.append(postalCodeTd);
            tr.append(provinceTd);
            tr.append(countryTd);
            tr.append(managerNameTd);
            tr.append(managerEmailTd);
            tr.append(managerPhoneTd);
            tr.append(createdAtTd);
            tr.append(optionsTd);

            // Append row to table body
            $('#warehousesTable tbody').append(tr);
        });
    }

});